#!/usr/bin/env python3
#-*- coding: utf-8 -*-

##This software is available to you under the terms of the GPL-3, see "/usr/share/common-licenses/GPL-3".
##Copyright:
##- Tomasz Makarewicz (makson96@gmail.com)

import os, importlib, urllib
from subprocess import check_output

from tools import update_do, download_engine, unpack_deb, sandbox

self_dir = os.path.dirname(os.path.abspath(__file__)) + "/"
recultis_dir = os.getenv("HOME") + "/.recultis/"
desk_dir = str(check_output(['xdg-user-dir', 'DESKTOP']))[2:-3] + "/"

#List available games
def get_game_list():
	print("Starting preparing game list")
	forbidden_dir = ["__pycache__"]
	all_dirs_and_files_list = os.listdir(self_dir)
	game_list = []
	for file_or_dir in all_dirs_and_files_list:
		if sandbox.ispresent(self_dir + file_or_dir) == (True, "dir"):
			if file_or_dir not in forbidden_dir:
				#Status -1 Checking for update...
				game_list.append([file_or_dir, -1])
	game_list = sorted(game_list)
	print("List of all supported games:")
	print(game_list)
	return game_list

#Install game
def install(game_name, shop, shop_login, shop_password):
	#Import game specific data
	game = importlib.import_module("games." + game_name + ".game")
	#Run install
	runtime_status_ok = False
	shop_status_ok = False
	print("Start installing " + game.full_name)
	print("Preparing directory structure")
	if sandbox.ispresent(game.install_dir)[0] == False:
		sandbox.mkdir(game.install_dir)
	if sandbox.ispresent(recultis_dir + "tmp")[0]:
		sandbox.rm(recultis_dir + "tmp")
	sandbox.mkdir(recultis_dir + "tmp")
	#Clean everything for status to work
	open(recultis_dir + "tmp/" + game_name + ".deb", 'a').close()
	if sandbox.ispresent(recultis_dir + "shops/steam/steam_log.txt")[0]:
		sandbox.mv(recultis_dir + "shops/steam/steam_log.txt", recultis_dir + "shops/steam/steam_log_old.txt")
	if sandbox.ispresent(recultis_dir + "shops/gog/gog_log.txt")[0]:
		sandbox.mv(recultis_dir + "shops/gog/gog_log.txt", recultis_dir + "shops/gog/gog_log_old.txt")
	if sandbox.ispresent(recultis_dir + "shops/gog/gog_log2.txt")[0]:
		sandbox.mv(recultis_dir + "shops/gog/gog_log2.txt", recultis_dir + "shops/gog/gog_log2_old.txt")
	print("Download and prepare runtime")
	runtime_link = update_do.get_link_list(["recultis-runtime"])[0]
	if sandbox.ispresent(recultis_dir + "runtime/recultis2/version_link.txt")[0]:
		runtime_version_file = open(recultis_dir + "runtime/recultis2/version_link.txt")
		runtime_version = runtime_version_file.read()
		if runtime_version == runtime_link:
			runtime_update_needed = 0
		else:
			runtime_update_needed = 1
	else:
		runtime_update_needed = 1
	if runtime_update_needed == 1:
		result = download_engine.download(runtime_link, recultis_dir + "tmp/recultis-runtime.deb")		
		unpack_deb.unpack_deb(recultis_dir + "tmp/", "recultis-runtime.deb")
		if sandbox.ispresent(recultis_dir + "runtime/")[0] == False:
			sandbox.mkdir(recultis_dir + "runtime/")
		if sandbox.ispresent(recultis_dir + "runtime/recultis2")[0]:
			sandbox.rm(recultis_dir + "runtime/recultis2")
		sandbox.mv(recultis_dir + "tmp/runtime/recultis2", recultis_dir + "runtime/recultis2")
		#Link libc for mono
		ldconfig_command = "ldconfig"
		if os.path.isfile("/sbin/ldconfig"):
			ldconfig_command = "/sbin/ldconfig"
		os.system(ldconfig_command + " -p | grep libc.so > $HOME/.recultis/tmp/libc_location.txt")
		with open(recultis_dir +"tmp/libc_location.txt") as f:
			for line in f:
				if "64" in line:
					libc_location = line.split(" => ")[1].rstrip()
		if sandbox.ispresent(recultis_dir + "runtime/recultis2/mono/libc.so")[0] == True:
			sandbox.rm(recultis_dir + "runtime/recultis2/mono/libc.so")
		sandbox.ln(libc_location, recultis_dir + "runtime/recultis2/mono/libc.so")
		#Mark installed version by coping link file
		version_link_file = open(recultis_dir + "runtime/recultis2/version_link.txt","w")
		version_link_file.write(runtime_link)
		version_link_file.close()
		runtime_status_ok = True
		print("Runtime ready")
	else:
		runtime_status_ok = True
		print("Runtime up to date")
	if runtime_status_ok == True:
		print("Download game content from digital distribution platform")
		if shop == "steam":
			from tools import steam
			print("Start Steam")
			shop_status_ok = steam.start(shop_login, shop_password, recultis_dir, game.s_appid, game.install_dir)
		elif shop == "gog":
			from tools import gog
			print("Start GOG")
			shop_status_ok = gog.start(shop_login, shop_password, recultis_dir, game.g_appid, game.install_dir)
		else:
			print("No data download, only engine update")
			shop_status_ok = True
	if shop_status_ok == True:
		print("Downloading game engine")
		link = update_do.get_link_list([game_name])[0]
		result = download_engine.download(link, recultis_dir + "tmp/" + game_name + ".deb")
		if result == 1:
			unpack_deb.unpack_deb(recultis_dir + "tmp/", game_name + ".deb")
			game.prepare_engine()
			#Mark installed version by coping link file
			version_link_file = open(game.install_dir + "/version_link.txt","w")
			version_link_file.write(link)
			version_link_file.close()
			sandbox.rm(recultis_dir + "tmp")
		else:
			error_file = open(recultis_dir + "error.txt", "w")
			error_file.write("Error: download game engine failed. Try again later.")
			error_file.close()

def uninstall(game_name):
	#Import game specific data
	game = importlib.import_module("games." + game_name + ".game")
	#Run uninstall
	print("Preparing list of files and directories to uninstall")
	uninstall_files_list = []
	for icon_name in game.icon_list:
		uninstall_files_list.append(os.getenv("HOME") + "/.local/share/icons/" + icon_name)
	for launch_file in game.launcher_list:
		uninstall_files_list.append(desk_dir + launch_file[0])
		uninstall_files_list.append(os.getenv("HOME") + "/.local/share/applications/" + launch_file[0])
	uninstall_dir_list = [game.install_dir]
	#Add game specific files to uninstall list
	uninstall_files_list.extend(game.uninstall_files_list)
	uninstall_dir_list.extend(game.uninstall_dir_list)
	#We need to add here some kind of sandbox
	print("Uninstalling files:")
	print(uninstall_files_list)
	for u_file in uninstall_files_list:
		if sandbox.ispresent(u_file)[0]:
			sandbox.rm(u_file, [os.getenv("HOME") + "/.local/share/icons/", os.getenv("HOME") + "/.local/share/applications/", os.getenv("HOME") + "/.config/"])
	print("Uninstalling directories:")
	print(uninstall_dir_list)
	for u_dir in uninstall_dir_list:
		if sandbox.ispresent(u_dir)[0]:
			sandbox.rm(u_dir)

def make_launchers(game_name):
	#Import game specific data
	game = importlib.import_module("games." + game_name + ".game")
	#Run creating launchers
	if sandbox.ispresent(os.getenv("HOME") + "/.local/share/icons")[0] == False:
		sandbox.mkdir(os.getenv("HOME") + "/.local/share/icons", [os.getenv("HOME") + "/.local/share/icons"])
	if sandbox.ispresent(os.getenv("HOME") + "/.local/share/applications/")[0] == False:
		sandbox.mkdir(os.getenv("HOME") + "/.local/share/applications/", [os.getenv("HOME") + "/.local/share/applications/"])
	print("Copy icon")
	for icon_name in game.icon_list:
		sandbox.cp(self_dir + game_name + "/" + icon_name, os.getenv("HOME") + "/.local/share/icons/" + icon_name, [os.getenv("HOME") + "/.local/share/icons/"])
	print("Make_launchers")
	for launcher in game.launcher_list:
		desktop_file = open(desk_dir + launcher[0], "w")
		desktop_file.write(launcher[1])
		desktop_file.close()
		os.chmod(desk_dir + launcher[0], 0o755)
		menu_file = open(os.getenv("HOME") + "/.local/share/applications/" + launcher[0] , "w")
		menu_file.write(launcher[1])
		menu_file.close()
		os.chmod(os.getenv("HOME") + "/.local/share/applications/" + launcher[0], 0o755)

def game_info(game_name, requested_list):
	#Import game specific data
	game_module = importlib.import_module("games." + game_name + ".game")
	#Run info gathering
	if sandbox.ispresent(game_module.install_dir + "/version_link.txt")[0]:
		version_file = open(game_module.install_dir + "/version_link.txt")
		version = version_file.read()
	else:
		version = "No proper install"
	deb_file_path = recultis_dir + "tmp/" + game_name + ".deb"
	return_list = []
	for requested_item in requested_list:
		if requested_item == "deb_file_path":
			return_list.append(deb_file_path)
		elif requested_item == "install_dir":
			return_list.append(game_module.install_dir)
		elif requested_item == "version":
			return_list.append(version)
		elif requested_item == "runtime_version":
			return_list.append(game_module.runtime_version)
		elif requested_item == "engine_size":
			try:
				link = update_do.get_link_list([game_name])[0]
				d = urllib.request.urlopen(link)
				url_s = int(d.getheaders()[2][1])
			except urllib.error.HTTPError:
				url_s = 0
			return_list.append(url_s)
		elif requested_item == "runtime_size":
			try:
				link = update_do.get_link_list(["recultis-runtime"])[0]
				d = urllib.request.urlopen(link)
				url_s = int(d.getheaders()[2][1])
			except urllib.error.HTTPError:
				url_s = 0
			return_list.append(url_s)
		else:
			raise ValueError("Unknown game info: " + requested_item)
	return return_list
